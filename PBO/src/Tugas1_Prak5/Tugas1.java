package Tugas1_Prak5;
import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
public class Tugas1 extends JFrame {
    private static final int FRAME_WIDTH = 300;
    private static final int FRAME_HEIGHT = 200;
    private static final int FRAME_X_ORIGIN = 150;
    private static final int FRAME_Y_ORIGIN = 250;
    private static final int BUTTON_WIDTH = 80;
    private static final int BUTTON_HEIGHT = 30;
    private JButton button;
    private JTextField jTextField1;
    private JTextField jTextField2;
    private JTextField hasil;
    private JLabel jlabel1;
    private JLabel jlabel2;
    private JLabel jlabel3;
    public static void main(String[] args){
        Tugas1 frame =  new Tugas1();
        frame.setVisible(true);
    }   
    public Tugas1(){
        Container contentPane = getContentPane();
        setSize ( FRAME_WIDTH, FRAME_HEIGHT );
        setResizable(true);
        setTitle("Luas Tanah");
        contentPane.setBackground(Color.PINK);
        setLocation( FRAME_X_ORIGIN, FRAME_Y_ORIGIN );
        contentPane.setLayout(null);
        
        jlabel1 = new JLabel("Panjang (m)");
        jlabel1.setBounds(10, 1,100,50);
        contentPane.add(jlabel1);
         
        jTextField1 = new JTextField();
        jTextField1.setBounds(100, 18, 150, 20);
        contentPane.add(jTextField1);
        
        jlabel2 = new JLabel("Lebar (m)");
        jlabel2.setBounds(10, 39, 100,50);
        contentPane.add(jlabel2);
        
        jTextField2 = new JTextField();
        jTextField2.setBounds(100, 55, 150, 20);
        contentPane.add(jTextField2);
        
        jlabel3 = new JLabel("Luas (m2)");
        jlabel3.setBounds(10, 75, 100,50);
        contentPane.add(jlabel3);
        
        hasil = new JTextField();
        hasil.setBounds(100, 90, 150, 20);
        contentPane.add(hasil);

        button = new JButton("Hitung");
        button.setBounds(100, 115, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(button);

        setDefaultCloseOperation(EXIT_ON_CLOSE);  
     
    button.addActionListener(new ActionListener(){
    int bil1, bil2, hasill;
    @Override
    public void actionPerformed (ActionEvent event){
    try{
        bil1 =  Integer.parseInt(jTextField1.getText());
        bil2 =  Integer.parseInt(jTextField2.getText());
        hasill =  bil1 * bil2;
        hasil.setText(Integer.toString(hasill));        
    }catch (Exception e) {
    JOptionPane.showMessageDialog (null,"Maaf, hanya Integer yang di perbolehkan!","ERROR" , JOptionPane.ERROR_MESSAGE);
      }
    }
    });
setVisible(true);
setLocationRelativeTo(null);
    }
}

